function CreateCar(vehId, plate, identifier, source, status)
	local self = {}

	self.vehId 		= vehId
	self.plate		= plate
	self.status		= status or 0
	self.owner 		= identifier
	self.owners		= {identifier}

	local class = {}

	class.get = function(k)
		return self[k]
	end

	class.set = function(k, v)
		self[k] = v
	end

	class.isOwner = function(identifier)
		local countOwner = 0
		for i=1, (#self.owners)do
			if(self.owners[i] == identifier)then
				countOwner = countOwner + 1
			end
		end
		if(countOwner > 0)then
			return true
		else
			return false
		end
	end

	class.giveKey = function(src, target)
		local srcId = GetPlayerIdentifiers(src)[1]
		local targetId = GetPlayerIdentifiers(target)[1]

		if(self.owner == srcId)then
			table.insert(self.owners, targetId)
			Notify(src, "Keys given to " .. GetPlayerName(target))
			Notify(target, "Keys received from " .. GetPlayerName(src))
		else
			Notify(src, "It is not possible")
		end
	end

	return class
end