local Cars = {}

RegisterServerEvent("ls:mainCheck")
AddEventHandler("ls:mainCheck", function(plate, vehicle, isPlayerInside)
	local src = source
	local identifier = GetPlayerIdentifiers(src)[1]
	local plate = string.lower(plate)

	local car = Cars[plate]
	if(car ~= nil)then
		if(car.get('status') ~= 'blocked')then
			if(car.isOwner(identifier) == true)then
				TriggerClientEvent('ls:lock', src)
			else
				Notify(src, "This is not your vehicle.")
			end
		else
			Notify(src, "The keys can't be found.")
		end
	else
		if(isPlayerInside)then
			if(TryToSteal() == true)then
				Cars[plate] = CreateCar(vehicle, plate, identifier, src)
				Notify(src, "You found the keys.")
			else
				Cars[plate] = CreateCar(vehicle, plate, identifier, src, "blocked")
				Notify(src, "You didn't find the keys.")
			end
		end
	end
end)

AddEventHandler('chatMessage', function(src, n, message)
	local args = {}
	for arg in message:gmatch("%S+") do 
		table.insert(args, arg)
	end

	args[1] = string.lower(args[1])

	if(args[1] == "/gk")then
		if(args[2])then
			local target_identifier = GetPlayerIdentifiers(args[2])[1]
			if(target_identifier)then
				local target = args[2]
				if(args[3])then
					local plate = string.lower(args[3])
					if(Cars[plate] ~= nil)then
						Cars[plate].giveKey(src, target)
					else
						TriggerClientEvent('chatMessage', src, '', {255, 255, 255}, '^1The vehicle with this plate does not exist.')
					end
				else
					TriggerClientEvent('chatMessage', src, '', {255, 255, 255}, '^1Third missing argument : ^0/gk <id> ^3<plate>')
				end
			else
				TriggerClientEvent('chatMessage', src, '', {255, 255, 255}, '^1Player not found.')
			end
		else
			TriggerClientEvent('chatMessage', src, '', {255, 255, 255}, '^1Second missing argument : ^0/gk ^3<id>^0 <plate>')
		end
		CancelEvent()
	end
end)